from transformers import GPT2LMHeadModel, GPT2Tokenizer
device = "cuda:1"

CKPT_DEMO = 'ckpts/checkpoint-1920500'

tokenizer = GPT2Tokenizer.from_pretrained(CKPT_DEMO, local_files_only=True)

model = GPT2LMHeadModel.from_pretrained(CKPT_DEMO).to(device)

tokenizer.push_to_hub('haining/gpt2-124m-wikipedia-1-epoch')
model.push_to_hub('haining/gpt2-124m-wikipedia-1-epoch')

