export CUDA_VISIBLE_DEVICES=0

# make ckpt and log directory if they do not exist
mkdir -p logs
mkdir -p wandb
mkdir -p ckpts

nohup python train.py \
--model_type gpt2 \
--config_name gpt2 \
--tokenizer_name gpt2 \
--dataloader_num_workers 4 \
--run_name gpt2_124m_wikipedia \
--save_total_limit 3 \
--num_train_epochs=1.0 \
--cache_dir ./cache \
--dataset_name wikipedia \
--dataset_config_name 20220301.en \
--per_device_train_batch_size 2 \
--do_train \
--fp16 \
--learning_rate=5e-5 \
--overwrite_output_dir \
--output_dir ./ckpts >logs/train.log 2>&1 &
